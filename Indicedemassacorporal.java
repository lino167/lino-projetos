import java.util.Scanner;

public class Indicedemassacorporal {
    public static void main(String[] args) {
        System.out.println("CÁLCULO DE INDÍCE CORPORAL");

        int opcao;
        do {
            System.out.println("1 - CALCULAR");
            System.out.print("O que voçê deseja fazer? (0 para sair): ");
    
            Scanner scanner = new Scanner(System.in);
            opcao = scanner.nextInt();
            processar(opcao);
        }while(opcao !=0);    
    }
    
    public static void processar( int opcao){
        switch(opcao){
            case 1: {
                Scanner scanner = new Scanner(System.in);
                System.out.println("PROCESSANDO CÁLCULO");

                System.out.print("Digite o peso: ");
                double peso = scanner.nextDouble();

                System.out.print("Digite o altura: ");
                double altura = scanner.nextDouble();

                double calculo = peso / (altura * altura);

                System.out.println("Resulado: "+ calculo);
                
                if (calculo < 18.5) {
                    System.out.println("Abaixo do Peso");
                    
                } else if(calculo >= 18.6 && calculo <= 24.9) {
                    System.out.println("Saúdavel");
                    
                }else if(calculo >= 25 && calculo <= 29.9) {
                    System.out.println("Peso em excesso");
                    
                }else if(calculo >= 30 && calculo <= 34.9) {
                    System.out.println("Obesidade Grau I");
                    
                }else if(calculo >= 35 && calculo <= 39.9) {
                    System.out.println("Obesidade Grau II"+ " (severa)");
                    
                }else {
                    System.out.println("Obesidade Grau III"+ " (mórbida)");
                    
                }
                 break;

            }
        }
    }
}
